import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {NavbarComponent} from './shared/navbar/navbar.component';
import {MatInputModule} from '@angular/material/input';
import {FooterComponent} from './shared/footer/footer.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {AppComponent} from './app.component';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './auth/login/login.component';
import {LayoutComponent} from './layout/layout.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {AppRoutingModule} from './app-routing.module';
import {RegisterComponent} from './auth/register/register.component';
import {UsecasesComponent} from './pages/about/usecases/usecases.component';
import {UsecaseComponent} from './pages/about/usecases/usecase/usecase.component';
import {RouterModule} from '@angular/router';
import {NgbCollapseModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProductListComponent} from './pages/product/product-list/product-list.component';
import {ProductEditComponent} from './pages/product/product-edit/product-edit.component';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {ShoppingListComponent} from './pages/shopping/shopping-list/shopping-list.component';
import {ShoppingEditComponent} from './pages/shopping/shopping-edit/shopping-edit.component';
import {ProductCreateComponent} from './pages/product/product-create/product-create.component';
import {ShoppingDetailsComponent} from './pages/shopping/shopping-details/shopping-details.component';
import {ShoppingCreateComponent} from './pages/shopping/shopping-create/shopping-create.component';
import {GroupListComponent} from './pages/groups/group-list/group-list.component';
import {GroupEditComponent} from './pages/groups/group-edit/group-edit.component';
import {GroupDetailsComponent} from './pages/groups/group-details/group-details.component';
import {GroupCreateComponent} from './pages/groups/group-create/group-create.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    UsecaseComponent,
    UsecasesComponent,
    LayoutComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    ProductListComponent,
    ProductEditComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    ProductCreateComponent,
    ShoppingDetailsComponent,
    ShoppingCreateComponent,
    GroupListComponent,
    GroupEditComponent,
    GroupDetailsComponent,
    GroupCreateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    MatSnackBarModule,
    MatMenuModule,
    MatListModule,
    NgbCollapseModule,
    MatFormFieldModule,
    MatTableModule,
    MatDividerModule,
    MatInputModule,
    MatButtonModule,
    HttpClientModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    NgbModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
}
