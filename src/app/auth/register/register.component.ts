import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandlerService} from '../../helpers/error-handler.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private errorHandler: ErrorHandlerService,
              private snackBar: MatSnackBar) {
    this.registerForm = formBuilder.group({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    console.log(this.registerForm.value);
    this.authService.register(this.registerForm.value).subscribe((data: any) => {
      console.log(data);
      this.snackBar.open('Successfully registered!', '', {
        duration: 3000
      });
      this.router.navigate(['/login']);
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.errorHandler.handleError('getById', err);
    });
  }


  ngOnInit(): void {
  }

}
