import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandlerService} from '../../helpers/error-handler.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  subscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private errorHandler: ErrorHandlerService) {
    this.loginForm = formBuilder.group({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      keepSigned: new FormControl('')
    });
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log(this.loginForm.value);
    console.log('form valid');
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    const keepSigned = this.loginForm.get('keepSigned').value;
    this.login(email, password, keepSigned);
  }

  public login(email, password, keepSigned): void {
    this.subscription = this.authService.login({email, password}).subscribe((data: any) => {
      console.log(`login successful, user: `, data);
      this.authService.saveUserToLocalStorage(data.user);
      this.authService.currentUser$.next(data.user);
      if (keepSigned) {
      }
      this.router.navigate(['/']).then(() => console.log('going to dashboard'));
      this.subscription.unsubscribe();
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.errorHandler.handleError('getById', err);
    });
  }
}
