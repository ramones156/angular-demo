import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CookiesHandlerService {

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.cookies.get('Token')}`
    })
  };

  constructor(private cookies: CookieService) {
  }

  public get(payload): string {
    return this.cookies.get(payload);
  }
}
