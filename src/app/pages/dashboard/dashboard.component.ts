import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  getUser(): void {
    if (this.auth.currentUser$ != null) {
      this.auth.getUserFromLocalStorage().subscribe(u => {
        console.log('Local storage', u);
      });
      console.log('Current user: ', this.auth.currentUser$);
    }
  }

  constructor(private auth: AuthService) {
  }

  ngOnInit(): void {
  }

}
