import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ShoppinglistService} from '../shoppinglist.service';
import {Router} from '@angular/router';
import {Group} from '../../groups/group.model';
import {GroupService} from '../../groups/group.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-shopping-create',
  templateUrl: './shopping-create.component.html',
  styleUrls: ['./shopping-create.component.css']
})
export class ShoppingCreateComponent implements OnInit, OnDestroy {
  listForm: FormGroup;
  groups: Group[];
  selGroup: string;
  subscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private groupService: GroupService,
    private shoppingListService: ShoppinglistService,
    private router: Router) {
    this.listForm = formBuilder.group({
      name: new FormControl('', [Validators.required]),
    });
  }

  getGroups(): void {
    this.subscription = this.groupService.getGroups().subscribe(res => {
      this.groups = res;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSubmit(): void {
    console.log('Selected: ', this.selGroup);
    this.shoppingListService.createList({
      name: this.listForm.get('name').value,
      group: this.selGroup
    }).subscribe(res => {
      console.log(res);
      this.router.navigate(['/list']);
    });
  }

  ngOnInit(): void {
    this.getGroups();
  }
}
