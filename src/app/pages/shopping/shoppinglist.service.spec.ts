import { TestBed } from '@angular/core/testing';

import { ShoppinglistService } from './shoppinglist.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatSnackBar} from '@angular/material/snack-bar';
import {OverlayModule} from '@angular/cdk/overlay';

describe('ShoppinglistService', () => {
  let service: ShoppinglistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, OverlayModule],
      providers: [MatSnackBar]
    });
    service = TestBed.inject(ShoppinglistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
