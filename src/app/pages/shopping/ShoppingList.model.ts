import {Product} from '../product/product.model';

export class ShoppingList {
  _id: string;
  name: string;
  products: Product[];
}
