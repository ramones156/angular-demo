import {Component, OnInit} from '@angular/core';
import {ShoppinglistService} from '../shoppinglist.service';
import {ShoppingList} from '../ShoppingList.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  lists: ShoppingList[];

  constructor(private listService: ShoppinglistService) {
  }

  getLists(): void {
    this.listService.getList().subscribe(res => {
      this.lists = res;
    });
  }

  ngOnInit(): void {
    this.getLists();
  }
}
