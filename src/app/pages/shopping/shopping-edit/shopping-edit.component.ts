import {Component, Input, OnInit} from '@angular/core';
import {ShoppingList} from '../ShoppingList.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @Input() details: ShoppingList;

  constructor() {
  }

  ngOnInit(): void {
    console.log('Details: ', this.details != null);
  }

  onSubmit(): void {

  }
}
