import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable, of} from 'rxjs';
import {Product} from './product.model';
import {catchError, tap} from 'rxjs/operators';
import {ShoppingList} from '../shopping/ShoppingList.model';
import {ErrorHandlerService} from '../../helpers/error-handler.service';
import {CookiesHandlerService} from '../../helpers/cookies-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private cookiesHandler: CookiesHandlerService,
    private errorHandler: ErrorHandlerService) {
  }

  getById(): Observable<Product> {
    return this.http.get<Product>('api/products/')
      .pipe(
        tap((res: Product) => {
          console.log('res: ', res);
        }),
      );
  }

  getItems(): Observable<Product[]> {
    return this.http.get<Product[]>('api/products')
      .pipe(
        tap((res: Product[]) => {
          console.log('res: ', res);
        }),
        catchError(error => of(this.errorHandler.handleError('getItems', error)))
      );
  }

  createProduct(payload): Observable<Product> {
    return this.http.post<Product>('api/products', payload)
      .pipe(
        tap((res: Product) => {
          console.log('res: ', res);
          this.snackBar.open('Product created!', '', {
            duration: 2000
          });
        }),
        catchError(error => of(this.errorHandler.handleError('createProduct', error)))
      );
  }

  addToList(payload, shopId): Observable<ShoppingList> {
    return this.http.put<ShoppingList>(`api/shop-list/${shopId}/add`, payload, this.cookiesHandler.httpOptions)
      .pipe(
        tap(() => {
          this.snackBar.open('Product added to list', '', {
            duration: 2000
          });
        }),
        catchError(error => of(this.errorHandler.handleError('addToList', error)))
      );
  }


  deleteItem(payload): Observable<Product> {
    return this.http.delete<Product>(`api/products/${payload._id}`)
      .pipe(
        catchError(error => of(this.errorHandler.handleError('deleteItem', error)))
      );
  }
}
