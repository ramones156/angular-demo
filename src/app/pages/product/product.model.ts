enum Categories{
  a = 1,
  b = 2
}

export class Product {
  _id: string;
  name: string;
  category: Categories[];
  price: number;
}
