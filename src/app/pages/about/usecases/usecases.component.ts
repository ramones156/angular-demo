import {Component, OnInit} from '@angular/core';
import {UseCase} from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})

export class UsecasesComponent implements OnInit {
  readonly GUEST = 'Bezoeker';
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly GROUP_OWNER = 'Groep eigenaar';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Registreren',
      description: 'Hiermee kan een gebruiker registreren.',
      scenario: [
        'Gebruiker navigeert naar de registratie pagina en vult alle gegevens in.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan logt de applicatie de bezoeker in en redirect hij naar het startscherm.',
      ],
      actor: this.GUEST,
      precondition: 'Geen',
      postcondition: 'De actor heeft een account en is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.GUEST,
      precondition: 'De actor heeft een account.',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-03',
      name: 'Een groep joinen',
      description: 'Hiermee kan een gebruiker zichzelf in een groep zetten',
      scenario: [
        'Gebruiker vult groepcode en wachtwoord in en klikt op \'Join\'.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar een detailscherm voor de groep.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'De gebruiker zit in de groep.',
    },
    {
      id: 'UC-04',
      name: 'Een Lijst aanmaken',
      description: 'Hiermee kan een gebruiker zichzelf in een groep zetten.',
      scenario: [
        'Gebruiker klikt op \'Lijst toevoegen\'.',
        'De applicatie valideert of de gebruiker hier toegang tot heeft.',
        'Indien alles correct is voegt de applicatie de lijst toe aan de groep.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker zit in een groep.',
      postcondition: 'De gebruiker heeft een lijst toegevoegd aan een groep.',
    },
    {
      id: 'UC-05',
      name: 'Een product aan een lijst toevoegen.',
      description: 'Hiermee kan een gebruiker een product in een lijst zetten.',
      scenario: [
        'Gebruiker gaat naar de lijst en klikt op toevoegen.',
        'De applicatie valideert of de gebruiker hier toegang tot heeft.',
        'Indien alles correct is voegt de applicatie het product toe aan de lijst.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De groep bevat een lijst.',
      postcondition: 'De lijst bevat een product.',
    },
    {
      id: 'UC-06',
      name: 'Een product markeren als gekocht.',
      description: 'Hiermee kan een gebruiker een product in een lijst markeren als gekocht',
      scenario: [
        'Gebruiker gaat naar de lijst en klikt op een checkmark bij een product.',
        'Gebruiker klikt op \'Save\'.',
        'De applicatie valideert of de gebruiker hier toegang tot heeft.',
        'Indien alles correct is update de applicatie het product.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De lijst bevat een product.',
      postcondition: 'Een product is nu geupdate.',
    },
    {
      id: 'UC-07',
      name: 'Een product verwijderen',
      description: 'Hiermee kan een gebruiker een product in een lijst verwijderen.',
      scenario: [
        'Gebruiker klikt op een kruisje bij een product.',
        'De applicatie valideert of de gebruiker hier toegang tot heeft.',
        'Indien alles correct is verwijdert de applicatie het product.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De lijst bevat een product.',
      postcondition: 'Een product is nu verwijdert.',
    },
    {
      id: 'UC-08',
      name: 'Een groep verwijderen',
      description: 'Hiermee kan de groep eigenaar de groep vewijderen',
      scenario: [
        'Eigenaar navigeert naar de groep en klikt op \'Groep verwijderen\'.',
        'Eigenaar voert als confirmatie het wachtwoord in.',
        'De applicatie valideert of de gebruiker hier toegang tot heeft.',
        'Indien alles correct is verwijdert de applicatie de groep.',
      ],
      actor: this.GROUP_OWNER,
      precondition: 'De gebruiker is een eigenaar van een groep.',
      postcondition: 'De groep is nu verwijdert.',
    },
  ];

  constructor() {
  }

  ngOnInit(): void {
  }
}
