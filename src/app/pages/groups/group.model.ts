import {User} from '../../auth/user.model';
import {ShoppingList} from '../shopping/ShoppingList.model';

export class Group {
  _id: string;
  name: string;
  owner: User;
  members: User[];
  shoplists: ShoppingList[];
}
