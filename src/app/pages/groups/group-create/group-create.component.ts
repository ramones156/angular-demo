import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GroupService} from '../group.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: ['./group-create.component.css']
})
export class GroupCreateComponent implements OnInit {
  groupForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private groupService: GroupService,
    private router: Router) {
    this.groupForm = formBuilder.group({
      name: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    this.groupService.createGroup(this.groupForm.value).subscribe(res => {
      console.log(res);
      this.router.navigate(['/groups']);
    });
  }

  ngOnInit(): void {
  }

}
