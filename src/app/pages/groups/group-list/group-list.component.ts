import {Component, OnDestroy, OnInit} from '@angular/core';
import {Group} from '../group.model';
import {GroupService} from '../group.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandlerService} from '../../../helpers/error-handler.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit, OnDestroy {
  groups: Group[];
  subscription: Subscription;

  constructor(private groupService: GroupService,
              private router: Router,
              private snackBar: MatSnackBar,
              private errorHandler: ErrorHandlerService) {
  }

  getGroups(): void {
    this.subscription = this.groupService.getGroups().subscribe(res => {
      this.groups = res;
    });
  }

  ngOnInit(): void {
    this.getGroups();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  delete(id): void {
    this.groupService.deleteGroup(id).subscribe(() => {
      this.snackBar.open('Group deleted!', '', {
        duration: 2000
      });
      this.getGroups();
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.errorHandler.handleError('getById', err);
    });
  }
}
