import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Group} from '../group.model';
import {GroupService} from '../group.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandlerService} from '../../../helpers/error-handler.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.css']
})
export class GroupEditComponent implements OnInit, OnDestroy {
  @Input() group: Group;
  groupForm: FormGroup;
  subscription: Subscription;
  id: string;

  constructor(private formBuilder: FormBuilder,
              private groupService: GroupService,
              private route: ActivatedRoute,
              private router: Router,
              private errorHandler: ErrorHandlerService,
              private snackBar: MatSnackBar) {
    this.groupForm = formBuilder.group({
      name: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    this.groupService.updateGroup(this.id, this.groupForm.value).subscribe(() => {
      this.snackBar.open('Group updated!', '', {
        duration: 2000,
      });
      this.router.navigate(['/groups/', this.id]);
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.errorHandler.handleError('getById', err);
    });
  }

  getDetails(): void {
    this.subscription = this.groupService.getDetails(this.id).subscribe((res) => {
      if (res != null) {
        this.group = res;
      }
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getDetails();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
