import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Group} from '../group.model';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupService} from '../group.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.css']
})
export class GroupDetailsComponent implements OnInit, OnDestroy {
  @Input() groupDetails: Group;
  subscription: Subscription;
  id: string;

  constructor(private route: ActivatedRoute, private router: Router, private groupService: GroupService) {
  }

  getDetails(): void {
    this.subscription = this.groupService.getDetails(this.id).subscribe((res) => {
      if (res != null) {
        this.groupDetails = res;
      }
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getDetails();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  edit(): void {
    this.router.navigate(['/groups/', this.id, '/edit']);
  }
}
