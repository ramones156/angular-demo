import {by, element} from 'protractor';
import {CommonPageObject} from '../common.po';

export class ProductPage extends CommonPageObject {
  getTitleText(): Promise<string> {
    return element(by.css('.navbar-brand')).getText() as Promise<string>;
  }
}
