import {browser, logging} from 'protractor';
import {ProductPage} from './products.po';

describe('This Angular webapp', () => {
  let page: ProductPage;
  beforeEach(() => {
    page = new ProductPage();
  });

  it('should display the expected title as navbar-brand', () => {
    page.navigateTo();
    expect(page.getTitleText()).toContain('Angular');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
