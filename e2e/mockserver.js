//
// This server will be started by Protractor in end-to-end tests.
// Add your API mocks for your specific project in this file.
//
const express = require("express");
const cors = require('cors')
const port = 3000;

let app = express();
let routes = require("express").Router();

app.use(cors())

routes.post("/api/login", (req, res, next) => {
  res.status(200).json({
    success: true,
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Im93bnNHcm91cHMiOlsiNWZjYTM3OWJkMzlhNjIyODcwMjBhMTY0IiwiNWZjYTM3YmViOWM1YzcxZTQ0NGJhM2VkIiwiNWZjYTM3YmZiOWM1YzcxZTQ0NGJhM2VlIiwiNWZjYTM3ZGFiOWM1YzcxZTQ0NGJhM2VmIiwiNWZjYTQ1NjI4Y2RlYWUwZTI4ZjllZWEwIiwiNWZjYTUyNDI2NGRhMzI1MTc0ODEzNGRlIiwiNWZjYTU0Y2NmNTdmOTcyZTJjMDY1MWUwIiwiNWZjYTVmMjI2OTFlOGYwYzUwZTFiM2Q5Il0sImluR3JvdXBzIjpbXSwiX2lkIjoiNWZjYTMyZGM4YmFlODY0ZGE4YzQ2MTk0IiwibmFtZSI6Im5hbWUyIiwiZW1haWwiOiJlMkBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYiQxMCROZFpZNlp5SmtFMWhNVkcyS1MvMWVlNTZwcFFlNjE1Y2s3NWkuaEJYV21iTmFGVHRFcy5DUyIsImNyZWF0ZWRBdCI6IjIwMjAtMTItMDRUMTM6MDA6MTIuNjc4WiIsInVwZGF0ZWRBdCI6IjIwMjAtMTItMDRUMTY6MDk6MDYuODIzWiIsIl9fdiI6MH0sImlhdCI6MTYwNzE4OTg4OCwiZXhwIjoxNjA3Nzk0Njg4fQ.PAYLaectP9TmmNmlI_J51EEIJy8vQvGRumYywCBYRis",
    userCredentials: {
      _id: "5fca32dc8bae864da8c46194",
      name: "Name",
      email: "e@mail.com",
      password: "$2b$10$NdZY6ZyJkE1hMVG2KS/1ee56ppQe615ck75i.hBXWmbNaFTtEs.CS",
      createdAt: "2020-12-04T13:00:12.678Z",
      updatedAt: "2020-12-04T16:09:06.823Z",
      ownsGroups: [],
      inGroups: [],
      __v: 0
    }
  });
});

// Finally add your routes to the app
app.use(routes);

app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});
