const mongoose = require('mongoose')

before(done => {
  mongoose.connect('mongodb://localhost/shoplist_test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  mongoose.connection
    .once('open', () => done())
    .on('error', err => console.warn('warn', err))
})

beforeEach((done) => {
  const {users, groups, products, shopLists} = mongoose.connection.collections;
  users.drop(() => {
    products.drop(() => {
      groups.drop(() => {
        // shopLists.drop(() => {
        done()
        // })
      })
    })
  })
})
