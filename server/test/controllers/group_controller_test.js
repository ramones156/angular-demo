const assert = require('assert')
const request = require('supertest')
const app = require('../../app')

const Group = require('../../models/group')

describe(('Group controller'), () => {
  let id;
  let user;
  let token;
  beforeEach((done) => {
    request(app)
      .post('/api/register')
      .send({
        name: "Naam",
        email: "e@mail.com",
        password: "password"
      })
      .end(() => {
        request(app)
          .post('/api/login')
          .send({
            email: "e@mail.com",
            password: "password"
          })
          .end((err, res) => {
            user = res.body.user
            token = res.body.token
            request(app)
              .post('/api/groups')
              .set({"Authorization": `Bearer ${token}`})
              .send({
                name: "Naam"
              })
              .end((err, res) => {
                id = res.body._id
                done()
              })
          })
      })
  })
  it('Post to /api/groups creates a new group', done => {
    Group.countDocuments().then(count => {
      request(app)
        .post('/api/groups')
        .set({"Authorization": `Bearer ${token}`})
        .send({
          name: "Naam"
        })
        .end(() => {
          Group.countDocuments().then(newCount => {
            assert(count + 1 === newCount)
            done()
          })
        })
    })
  })
  it('Get to /api/groups shows your groups', done => {
    request(app)
      .get(`/api/groups`)
      .set({"Authorization": `Bearer ${token}`})
      .end((err, res) => {
        assert(res.body != null)
        assert(res.body[0].name === 'Naam')
        done()
      })
  })
})
