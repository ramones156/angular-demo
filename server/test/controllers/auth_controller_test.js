const assert = require('assert')
const request = require('supertest')
const app = require('../../app')

const User = require('../../models/user')

describe(('Authentication controller'), () => {
  it('Post to /api/register creates a new user', done => {
    User.countDocuments().then(count => {
      request(app)
        .post('/api/register')
        .send({
          name: 'jeff',
          email: 'e@mail.com',
          password: 'password'
        })
        .end(() => {
          User.countDocuments().then(newCount => {
            assert(count + 1 === newCount)
            done()
          })
        })
    })
  })
  it('Post to /api/login returns a user', done => {
    request(app)
      .post('/api/register')
      .send({
        name: 'jeff',
        email: 'e@mail.com',
        password: 'password'
      }).then(() => {
      request(app)
        .post('/api/login')
        .send({
          email: 'e@mail.com',
          password: 'password'
        })
        .end((err, res) => {
          assert(res !== null)
          done()
        })
    })
  })
})
