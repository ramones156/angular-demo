const Product = require('../models/product')


module.exports = {
  index(req, res, next) {
    Product.find({})
      .then(drivers => res.send(drivers))
      .catch(next)
  },
  details(req, res, next) {
    Product.findById({_id: req.params.id})
      .then(product => res.send(product))
      .catch(next)
  },
  create(req, res, next) {
    Product.create(req.body)
      .then(product => res.send(product))
      .catch(next)
  },
  put(req, res, next) {
    Product.findByIdAndUpdate({_id: req.params.id}, req.body)
      .then(() => Product.findById({_id: req.params.id}))
      .then(product => res.send(product))
      .catch(next)
  },
  delete(req, res, next) {
    Product.findByIdAndDelete({_id: req.params.id})
      .then(product => res.status(200).send({
        message: 'Product deleted.',
        body: product
      }))
      .catch(next)
  }
}
