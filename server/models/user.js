const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken");
const GroupSchema = require('../models/group').schema

const UserSchema = new Schema({
    name: {
      type: String,
      validate: {
        validator: (name) => name.length > 2,
        message: 'Name must be longer than 2 characters.'
      },
      required: [true, 'Name is required']
    },
    password: {
      type: String,
      required: [true, 'Password is required']
    },
    email: {
      type: String,
      required: [true, 'Email is required'],
      unique: true
    },
    groups: [GroupSchema]
  },
  {
    timestamps: true,
  })

UserSchema.pre('remove', (next) => {
  const OwnedGroup = mongoose.model('group');
  OwnedGroup.deleteMany({_id: {$in: this.owner}})
    .then(() => next())
})

UserSchema.methods.hashPassword = (password) => {
  return bcrypt.hashSync(password, 10);
}

UserSchema.methods.compareUserPassword = (inputtedPassword, hashedPassword) => {
  return bcrypt.compare(inputtedPassword, hashedPassword)
}

UserSchema.methods.generateJwtToken = (payload, secret, expires) => {
  return jwt.sign(payload, secret, expires)
}
module.exports = mongoose.model('user', UserSchema)
