const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  categories: [],
  price: {
    type: Number,
    required: [true, 'Price is required']
  },

})
