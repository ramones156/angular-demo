const app = require("../server/app");
const debug = require("debug")("node-angular");
const http = require("http");
const neo_driver = require('./neo')

const normalizePort = val => {
  const port = parseInt(val, 10);
  if (isNaN(port)) {
    // named pipe
    return val;
  }
  if (port >= 0)
    return port;
  return false;
};
function neo(dbName) {
  try {
    neo_driver.connect(dbName)
    console.log(`connection to neo DB ${dbName} established`)
  } catch (err) {
    console.error(err)
  }
}

const onError = error => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof port === "string" ? "pipe " + port : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof port === "string" ? "pipe " + port : "port " + port;
  debug("Listening on " + bind);
};

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);
neo(process.env.NEO4J_PROD_DB)
const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);
server.listen(port);
